#imports de Django
from django.contrib import admin
from django.urls import path, include

#imports de DRF
from rest_framework_simplejwt import views as jwt_views
from rest_framework import routers

#imports de mi app
from . import views

app_name = "api"

#uso los enrutadores de DRF para los ViewSet
router = routers.DefaultRouter()
router.register(r'products', views.ProductViewSet)
router.register(r'orders', views.OrderViewSet)
router.register(r'order_details', views.OrderDetailViewSet)

urlpatterns = [
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('', include(router.urls)),
]