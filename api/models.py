from django.db import models


#modelo de Product
class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    price = models.FloatField()
    stock = models.IntegerField()

    def __str__(self):
        return "[" + str(self.id) + "] " + self.name + " P=$" + str(self.price) + " S= " + str(self.stock)

#modelo para Order
class Order(models.Model):
    id = models.AutoField(primary_key=True)
    date_time = models.DateTimeField()

    def total(self):
        total = 0
        for order_detail in self.orderdetail.all():
            total += order_detail.quantity * order_detail.product.price
        return total

    def __str__(self):
        return "[" + str(self.id) + "] " + str(self.date_time)

#modelo para OrderDetail
class OrderDetail(models.Model):

    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="orderdetail")
    quantity = models.IntegerField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return "[" + str(self.order.id) + "] " + str(self.product.name) + " Q= " + str(self.quantity)