#imports de Django
from django.db.models.signals import pre_delete, pre_save
from django.dispatch import receiver
from django.core.exceptions import ObjectDoesNotExist

#imports de mi app
from .models import OrderDetail, Product

#signal that avoid creating a Product with price or stock less or equal to zero
@receiver(pre_save, sender=Product)
def product_pre_save_control(sender, instance, **kwargs):
    if instance.price <= 0:
        raise ValueError("Price cannot be negative or 0 (zero)")
    if instance.stock <= 0:
        raise ValueError("Stock cannot be negative or 0 (zero)")


@receiver(pre_delete, sender=OrderDetail)
def order_details_pre_delete(sender, instance, **kwargs):
    instance.product.stock += instance.quantity
    instance.product.save()

@receiver(pre_save, sender=OrderDetail)
def order_details_pre_save(sender, instance, **kwargs):
    try:
        old_instance = OrderDetail.objects.get(id=instance.id)
        if old_instance.quantity and old_instance.quantity != instance.quantity:
            difference = old_instance.quantity - instance.quantity 
            if difference > 0:
                instance.product.stock += difference
            else:
                instance.product.stock -= abs(difference)
            instance.product.save()
        else:
            instance.product.stock = instance.product.stock - instance.quantity
            instance.product.save()
        old_instance.quantity = instance.quantity
    except ObjectDoesNotExist:
        instance.product.stock = instance.product.stock - instance.quantity
        instance.product.save()
