#imports de Django
from django.test import TestCase
from django.contrib.auth.models import User
from django.utils import timezone

#imports de Python
import json, datetime

#imports de DRF
from rest_framework.test import APIClient
from rest_framework import status

#imports de mi app
from api.models import Product, Order, OrderDetail


class ProductTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='federico',
            password='1234',
        )
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

    #PRODUCT TEST 1 - #1
    def test_crear_product(self):
        data = {
            'name': 'Test Product',
            'price': 10.0,
            'stock': 10,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Product.objects.count(), 1)
        self.assertEqual(Product.objects.get().name, 'Test Product')
    
    #PRODUCT TEST 2 - #2
    def test_crear_product_sin_nombre(self):
        data = {
            'price': 10.0,
            'stock': 10,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)

    #PRODUCT TEST 3 - #3
    def test_crear_product_sin_precio(self):
        data = {
            'name': 'Test Product',
            'stock': 10,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)
    
    #PRODUCT TEST 4 - #4
    def test_crear_product_sin_stock(self):
        data = {
            'name': 'Test Product',
            'price': 10.0,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)

    #PRODUCT TEST 5 - #5
    def test_crear_product_con_precio_negativo(self):
        data = {
            'name': 'Test Product',
            'price': -10.0,
            'stock': 10,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)
    
    #PRODUCT TEST 6 - #6
    def test_crear_product_con_stock_negativo(self):
        data = {
            'name': 'Test Product',
            'price': 10.0,
            'stock': -10,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)

    #PRODUCT TEST 7 - #7
    def test_crear_product_con_precio_cero(self):
        data = {
            'name': 'Test Product',
            'price': 0.0,
            'stock': 10,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)

    #PRODUCT TEST 8 - #8
    def test_crear_product_con_stock_cero(self):
        data = {
            'name': 'Test Product',
            'price': 10.0,
            'stock': 0,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)
    
    #PRODUCT TEST 9 - #9
    def test_crear_product_con_precio_y_stock_negativo(self):
        data = {
            'name': 'Test Product',
            'price': -10.0,
            'stock': -10,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)

    #PRODUCT TEST 10 - #10
    def test_crear_product_con_precio_y_stock_cero(self):
        data = {
            'name': 'Test Product',
            'price': 0.0,
            'stock': 0,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)
    
    #PRODUCT TEST 11 - #11
    def test_crear_product_con_precio_negativo_stock_cero(self):
        data = {
            'name': 'Test Product',
            'price': -10.0,
            'stock': 0,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)

    #PRODUCT TEST 12 - #12
    def test_crear_product_con_precio_cero_stock_negativo(self):
        data = {
            'name': 'Test Product',
            'price': 0.0,
            'stock': -10,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)
    
    #PRODUCT TEST 13 - #13
    def test_crear_product_con_precio_y_stock_negativo(self):
        data = {
            'name': 'Test Product',
            'price': -10.0,
            'stock': -10,
        }
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)

    def test_crear_product_stock_negativo(self):
        try: 
            product = Product.objects.create(name='Test Product', price=10.0, stock=-10)
        except ValueError:
            self.assertEqual(Product.objects.count(), 0)

    def test_crear_product_precio_negativo(self):
        try:
            product = Product.objects.create(name='Test Product', price=-10.0, stock=10)
        except ValueError:
            self.assertEqual(Product.objects.count(), 0)

    #PRODUCT TEST 14 - #14
    def test_editar_product(self):
        product = Product.objects.create(name='Test Product', price=10.0, stock=10)
        data = {
            'name': 'Test Product Edited',
            'price': 20.0,
            'stock': 20,
        }
        response = self.client.put('/api/products/{}/'.format(product.id), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Product.objects.count(), 1)
        self.assertEqual(Product.objects.get().name, 'Test Product Edited')
        self.assertEqual(Product.objects.get().price, 20.0)
        self.assertEqual(Product.objects.get().stock, 20)

    #PRODUCT TEST 15 - #15
    def test_borrar_product(self):
        product = Product.objects.create(name='Test Product', price=10.0, stock=10)
        response = self.client.delete('/api/products/{}/'.format(product.id), format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Product.objects.count(), 0)
    
class OrderTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='federico',
            password='1234',
        )
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        self.product = Product.objects.create(
            name = 'Test Product',
            price = 10.0,
            stock = 10,
        )
    
    #ORDER TEST 1 - #16
    def test_crear_order(self):
        data = {
            'date_time' : '2022-02-20T00:00:00Z',
            "orderdetail": [
                {
                    "product": self.product.id,
                    "quantity": 2
                }
            ]
        }
        response = self.client.post('/api/orders/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Order.objects.count(), 1)
        self.assertEqual(Order.objects.get().date_time, datetime.datetime(2022, 2, 20, 0, 0, 0, tzinfo=timezone.utc))

    #ORDER TEST 2 - #17
    def test_crear_order_vacia(self):
        data = {
            'date_time' : '2022-02-20T00:00:00Z',
        }
        response = self.client.post('/api/orders/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    #ORDER TEST 3 - #18
    def test_crear_order_sin_date_time(self):
        data = {}
        response = self.client.post('/api/orders/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Order.objects.count(), 0)

    #ORDER TEST 4 - #19
    def test_editar_order_date_time(self):
        order = Order.objects.create(date_time=datetime.datetime(2022, 2, 20, 0, 0, 0, tzinfo=timezone.utc))
        data = {
            'date_time' : '2022-02-20T00:00:00Z',
        }
        response = self.client.put('/api/orders/{}/'.format(order.id), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Order.objects.count(), 1)
        self.assertEqual(Order.objects.get().date_time, datetime.datetime(2022, 2, 20, 0, 0, 0, tzinfo=timezone.utc))

    #ORDER TEST 5 - #20
    def test_borrar_order(self):
        order = Order.objects.create(date_time=datetime.datetime(2022, 2, 20, 0, 0, 0, tzinfo=timezone.utc))
        response = self.client.delete('/api/orders/{}/'.format(order.id), format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Order.objects.count(), 0)
    
class OrderDetailTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='federico',
            password='1234',
        )
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        self.order = Order.objects.create(
            date_time = datetime.datetime(2022, 2, 20, 0, 0, 0, tzinfo=timezone.utc),
        )
        self.product = Product.objects.create(
            name = 'Test Product',
            price = 10.0,
            stock = 10,
        )
    
    #ORDER DETAIL TEST 1 - #17
    def test_crear_order_detail(self):
        data = {
            'order': self.order.id,
            'product': self.product.id,
            'quantity': 5,
        }
        response = self.client.post('/api/order_details/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(OrderDetail.objects.count(), 1)
        self.assertEqual(OrderDetail.objects.get().order, self.order)
        self.assertEqual(OrderDetail.objects.get().product, self.product)
        self.assertEqual(OrderDetail.objects.get().quantity, 5)

    #ORDER DETAIL TEST 2 - #18
    def test_crear_order_detail_sin_order(self):
        data = {
            'product': self.product.id,
            'quantity': 5,
        }
        response = self.client.post('/api/order_details/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(OrderDetail.objects.count(), 0)
    
    #ORDER DETAIL TEST 3 - #19
    def test_crear_order_detail_sin_producto(self):
        data = {
            'order': self.order.id,
            'quantity': 5,
        }
        response = self.client.post('/api/order_details/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(OrderDetail.objects.count(), 0)

    #ORDER DETAIL TEST 4 - #20
    def test_crear_order_detail_sin_cantidad(self):
        data = {
            'order': self.order.id,
            'product': self.product.id,
        }
        response = self.client.post('/api/order_details/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(OrderDetail.objects.count(), 0)
    
    #ORDER DETAIL TEST 5 - #21
    def test_crear_order_detail_con_cantidad_negativa(self):
        data = {
            'order': self.order.id,
            'product': self.product.id,
            'quantity': -10,
        }
        response = self.client.post('/api/order_details/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(OrderDetail.objects.count(), 0)
    
    #ORDER DETAIL TEST 6 - #22
    def test_crear_order_detail_con_cantidad_cero(self):
        data = {
            'order': self.order.id,
            'product': self.product.id,
            'quantity': 0,
        }
        response = self.client.post('/api/order_details/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(OrderDetail.objects.count(), 0)

    #ORDER DETAIL TEST 13 - #23
    def test_crear_order_detail_con_id_erroneo(self):
        data = {
            'order': self.order.id,
            'product': 9999999,
            'quantity': 5,
        }
        response = self.client.post('/api/order_details/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(OrderDetail.objects.count(), 0)
    
    #ORDER DETAIL TEST 14 - #24
    def test_crear_order_detail_con_order_erronea(self):
        data = {
            'order': 9999999,
            'product': self.product.id,
            'quantity': 5,
        }
        response = self.client.post('/api/order_details/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(OrderDetail.objects.count(), 0)

    #ORDER DETAIL TEST 15 - #25
    def test_editar_order_detail(self):
        #Creo un nuevo producto con stock suficiente
        product_edit = Product.objects.create(
            name='Product to edit',
            stock=50,
            price=100
        )
        data = {
            'order': self.order.id,
            'product': product_edit.id,
            'quantity': 10,
        }
        response = self.client.post('/api/order_details/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(OrderDetail.objects.count(), 1)
        self.assertEqual(OrderDetail.objects.get().order, self.order)
        self.assertEqual(OrderDetail.objects.get().product, product_edit)
        self.assertEqual(OrderDetail.objects.get().quantity, 10)
        #Edito el order detail
        data = {
            'order': self.order.id,
            'product': product_edit.id,
            'quantity': 20,
        }
        response = self.client.put('/api/order_details/{}/'.format(OrderDetail.objects.get().pk), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(OrderDetail.objects.count(), 1)
        self.assertEqual(OrderDetail.objects.get().order, self.order)
        self.assertEqual(OrderDetail.objects.get().product, product_edit)
        self.assertEqual(OrderDetail.objects.get().quantity, 20)

    #ORDER DETAIL TEST 16 - #26
    def test_borrar_order_detail(self):
        product_test = Product.objects.create(
            name='Product to edit',
            stock=50,
            price=100
        )
        data = {
            'order': self.order.id,
            'product': product_test.id,
            'quantity': 10,
        }
        response = self.client.post('/api/order_details/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(OrderDetail.objects.count(), 1)
        response = self.client.delete('/api/order_details/{}/'.format(OrderDetail.objects.get().pk), format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(OrderDetail.objects.count(), 0)