from django.contrib import admin
from .models import Product, Order, OrderDetail

# Registro todos los modelos que voy a mostrar en el admin

class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'price', 'stock')
    list_filter = ('name', 'price', 'stock')
    search_fields = ('name', 'price', 'stock')
    ordering = ('name', 'price', 'stock')

class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'date_time')
    list_filter = ('date_time',)
    search_fields = ('date_time',)
    ordering = ('date_time',)

class OrderDetailAdmin(admin.ModelAdmin):
    list_display = ('id', 'order','quantity', 'product')
    list_filter = ('quantity', 'product')
    search_fields = ('quantity', 'product')
    ordering = ('quantity', 'product')

admin.site.register(Product, ProductAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(OrderDetail, OrderDetailAdmin)