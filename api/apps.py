from django.apps import AppConfig


class ApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api'
    def ready(self):
        from .signals import order_details_pre_delete
        import logging
        logger = logging.getLogger(__name__)
        logger.info("ENTERING SIGNAL")
