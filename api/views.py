#imports de Django
from django.shortcuts import render

#imports de DRF
from rest_framework_simplejwt import views as jwt_views
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

#imports de mi app
from .models import Product, Order, OrderDetail
from .serializers import ProductSerializer, OrderSerializer, OrderDetailSerializer

#visualizacion para Product
class ProductViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = ProductSerializer
    queryset = Product.objects.all()

#visualizacion para Order
class OrderViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

#visualizacion para OrderDetail
class OrderDetailViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = OrderDetailSerializer
    queryset = OrderDetail.objects.all()
