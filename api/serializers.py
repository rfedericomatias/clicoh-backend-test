#imports de DRF
from rest_framework import serializers
from rest_framework.response import Response

#imports de Django
from django.shortcuts import get_object_or_404

#imports de Python
import requests

#imports de mi app
from .models import Product, Order, OrderDetail

#serializador para Product
class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['id', 'name', 'price', 'stock']

    #defino un validador para que el precio y el stock sean mayores a 0
    def validate(self, data):
        if data['price'] <= 0:
            raise serializers.ValidationError("Price cannot be negative or 0 (zero)")
        if data['stock'] <= 0:
            raise serializers.ValidationError("Stock cannot be negative or 0 (zero)")
        return data

#serializador para OrderDetail
class OrderDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderDetail
        fields = ['order', 'quantity', 'product']

    #overwrite create method to check if Product is already in the Order
    def create(self, validated_data):
        #obtengo todos los OrderDetails para esta Order y verifico que el producto este en el Order
        order_details = OrderDetail.objects.filter(order=validated_data['order'])
        for order_detail in order_details:
            if order_detail.product == validated_data['product']:
                raise serializers.ValidationError("Product already in the Order")
        #check if the product has enough stock
        if validated_data['product'].stock < validated_data['quantity']:
            raise serializers.ValidationError("Product does not have enough stock")
        if validated_data['quantity'] <= 0:
            raise serializers.ValidationError("Quantity should be more than 0")
        return super().create(validated_data)
    
    def update(self, instance, validated_data):
        #check if the product has changed
        if instance.product != validated_data['product']:
            #check if the product has enough stock
            if validated_data['product'].stock < validated_data['quantity']:
                raise serializers.ValidationError("Product does not have enough stock")
            if validated_data['quantity'] <= 0:
                raise serializers.ValidationError("Quantity should be more than 0")
        else:
            #check if the product has enough stock
            if validated_data['product'].stock < validated_data['quantity']:
                raise serializers.ValidationError("Product does not have enough stock")
            if validated_data['quantity'] <= 0:
                raise serializers.ValidationError("Quantity should be more than 0")
        return super().update(instance, validated_data)
   

#serializer que uso para representar mas datos cuando se visualiza la Order
class OrderDetailChildSerializer(serializers.ModelSerializer):
    
        class Meta:
            model = OrderDetail
            fields = ['quantity', 'product']
    
        def to_representation(self, instance):
            return {
            'id': instance.product.id,
            'name': instance.product.name,
            'price': instance.product.price,
            'quantity': instance.quantity,
            'subtotal': instance.quantity * instance.product.price
            }

#serializador para Order 
class OrderSerializer(serializers.ModelSerializer):

    orderdetail = OrderDetailChildSerializer(many=True, read_only=True)
    total = serializers.SerializerMethodField()
    total_usd = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = ['id', 'date_time', 'orderdetail', 'total', 'total_usd']
        read_only_fields = ['id', 'total', 'total_usd']

    #sobreescribo el create para poder manipular los OrderDetails al agregar una Order
    def create(self, validated_data):
        try:
            #creo una una Order
            order = Order.objects.create(date_time=validated_data['date_time'])
            #creo nuevos OrderDetail con los datos de cada producto
            for order_detail in self.initial_data["orderdetail"]:
                product=get_object_or_404(Product, pk=order_detail['product'])
                #verifico si existe un Product repetido en la Order
                order_detail_exist = OrderDetail.objects.filter(order=order, product=product)
                if order_detail_exist:
                    raise serializers.ValidationError("Product already in the Order")
                else:
                    OrderDetail.objects.create(order=order, product=product, quantity=order_detail['quantity'])
            return order
        except KeyError:
            raise serializers.ValidationError("Order Detail Cannot Be Empty")

    #sobreescribo el update para poder manipular los OrderDetails al modificar una Order
    def update(self, instance, validated_data):
        #obtengo la Order
        order = get_object_or_404(Order, pk=instance.id)
        #Obtengo las OrderDetails
        order_details = OrderDetail.objects.filter(order=order)
        #Obtengo las nuevas solicitudes de OrderDetail
        try:
            new_order_details = self.initial_data['orderdetail']
            for nod in new_order_details:
                #actualizo OrderDetail con el Product y el Quantity segun existan
                product=get_object_or_404(Product, pk=nod['product'])
                order_detail_exist = OrderDetail.objects.filter(order=order, product=product)
                if order_detail_exist:
                    order_detail_exist.update(quantity=nod['quantity'])
                #si no existe se crea una nueva OrderDetail
                else:
                    OrderDetail.objects.create(order=order, product=product, quantity=nod['quantity'])
            #Si el usuario saco OrderDetail de la Order entonces lo tengo que sacar del sistema
            for od in order_details:
                if od.product.id not in [nod['product'] for nod in new_order_details]:
                    od.delete()
            return order
        except KeyError:
            #update only date_time of the Order
            order.date_time = validated_data['date_time']
            order.save()
            return order


    #expongo el metodo que calcula el total de la orden
    def get_total(self, instance):
        return instance.total()

    #expongo un metodo que toma la api de dolarsi y convierte el total a dolares blue
    def get_total_usd(self, instance):
        url = 'https://www.dolarsi.com/api/api.php?type=valoresprincipales'
        dolar = 1
        r = requests.get(url)
        if r.status_code == 200:
            json = r.json()
            #la api devuelve un json pero tiene una lista que tengo que recorrer
            for index, value in enumerate(json):
                #busco el dolar blue para poder hacer el calculo
                if json[index]['casa']['nombre'] == 'Dolar Blue':
                    #tengo que convertir a float porque la api devuelve un string que encima tiene una coma en vez de un punto 
                    dolar = float(json[index]['casa']['venta'].replace(",", "."))
        #devuelvo un valor redondeado a 2 decimales para que sea mas legible 
        return round(instance.total() / dolar, 2)